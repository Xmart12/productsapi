﻿using System.Net;
using System.Net.Mail;

namespace ProductsAPI.Helpers
{
    public class MailService
    {
        public static string UserMail { get; set; }
        public static string DisplayName { get; set; }
        public static string UserPassword { get; set; }
        public static string FromMail { get; set; }
        public static string MailServer { get; set; }
        public static int MailPort { get; set; }
        public static bool EnableSSL { get; set; }


        //Fucntion for Send Mail
        public static bool SendMail(string to, string subject, string message)
        {
            MailMessage m = new MailMessage();
            SmtpClient smtp = new SmtpClient();

            try
            {
                m.From = new MailAddress(FromMail, DisplayName);
                m.To.Add(new MailAddress(to));
                m.Subject = subject;
                m.Body = message;
                m.IsBodyHtml = true;

                smtp.Host = MailServer;
                smtp.Port = MailPort;
                smtp.Credentials = new NetworkCredential(UserMail, UserPassword);
                smtp.EnableSsl = EnableSSL;
                smtp.Send(m);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}

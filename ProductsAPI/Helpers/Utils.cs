﻿using Microsoft.AspNetCore.Http;

namespace ProductsAPI.Helpers
{
    public class Utils
    {
        //Function to get de BaseURL
        public static string GetBaseUrl(HttpRequest request)
        {
            var host = request.Host.ToUriComponent();

            var pathBase = request.PathBase.ToUriComponent();

            return $"{request.Scheme}://{host}{pathBase}";
        }
    }
}

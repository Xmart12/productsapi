﻿using Microsoft.AspNetCore.Identity;

namespace ProductsAPI.Models
{
    public class ApplicationUser : IdentityUser
    {
    }
}

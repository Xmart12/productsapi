﻿using System;   
using System.ComponentModel.DataAnnotations;

namespace ProductsAPI.Models
{

    public class RegisterModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }
    }


    public class LoginModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }


    public class ConfirmAccountModel
    {
        [Required]
        public string Email { get; set; }
    }


    public class ResetPasswordModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Code { get; set; }
    }


    public class ChangeRole
    {
        [Required]
        public string Email { get; set; }
        [Required]
        public string Role { get; set; }
    }


    public static class RoleType
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}

﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProductsAPI.Helpers;
using ProductsAPI.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProductsAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/products")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    public class ProductsController : Controller
    {
        //Database Context
        private readonly ApplicationDbContext db;

        //Enviroment Parameters
        private readonly IHostingEnvironment env;


        //Constructor
        public ProductsController(ApplicationDbContext context, IHostingEnvironment environment)
        {
            db = context;
            env = environment;
        }
        


        //List of Products
        // GET: api/products
        [HttpGet]
        public IActionResult List(string q, string sort, int? page, int? pagesize)
        {
            //Get Products List
            var products = db.Products.ToList();

            //Query search string
            if (!string.IsNullOrWhiteSpace(q))
            {
                //
                //q = q.ToLower();

                products = products.Where(w => w.Sku.Contains(q) || w.Name.Contains(q)).ToList();
            }

            //Sortable string
            switch (sort)
            {
                case "name":
                    products = products.OrderBy(o => o.Name).ToList();
                    break;
                case "name_desc":
                    products = products.OrderByDescending(o => o.Name).ToList();
                    break;
                case "qty":
                    products = products.OrderBy(o => o.Quantity).ToList();
                    break;
                case "qty_desc":
                    products = products.OrderByDescending(o => o.Quantity).ToList();
                    break;
                case "price":
                    products = products.OrderBy(o => o.Price).ToList();
                    break;
                case "price_desc":
                    products = products.OrderByDescending(o => o.Price).ToList();
                    break;
            }

            //Get the view model
            List<ProductViewModel> productList = products.Select(s => new ProductViewModel()
            {
                Sku = s.Sku,
                Name = s.Name,
                Description = s.Description,
                Quantity = s.Quantity,
                Price = s.Price,
                URLImage = s.URLImage
            }).ToList();

            //Paging dada
            PaginatedList<ProductViewModel> data = new PaginatedList<ProductViewModel>(productList, (page ?? 1), (pagesize ?? 5));

            //Set Paging Metadata
            var pagingdata = new { data.TotalCount, data.PageSize, data.TotalPages, data.PageIndex, data.HasNextPage, data.HasPreviousPage };
            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(pagingdata));

            //Create Paging Data Content
            data = data.Create();

            //Return Data
            return Ok(data);
        }


        //Especified product by sku id
        // GET: api/products/{id}
        [HttpGet("{id}", Name = "GetProduct")]
        public IActionResult GetProduct(string id)
        {
            //Get product db
            var product = db.Products.FirstOrDefault(f => f.Sku == id);

            //Product not found
            if (product == null)
            {
                return NotFound();
            }

            //Get product model
            var productmodel = new ProductViewModel()
            {
                Sku = product.Sku,
                Name = product.Name,
                Description = product.Description,
                Quantity = product.Quantity,
                Price = product.Price,
                URLImage = product.URLImage
            };

            //Return product
            return Ok(productmodel);
        }


        // Create new product
        // POST: api/products
        [HttpPost]
        public IActionResult CreateProduct([FromForm] ProductViewModel productmod)
        {
            string urlImage = null;

            //Model is not valid
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Verify SKU
            if (db.Products.Count(c => c.Sku.Equals(productmod.Sku)) > 0)
            {
                ModelState.AddModelError("Create Error", "Product sku already exists");
                return BadRequest(ModelState);
            }

            //Verify file exists
            if (productmod.Image != null)
            {
                if (productmod.Image.Length > 0)
                {
                    //Save file
                    var fileName = Path.GetFileName(productmod.Image.FileName);
                    var filePath = Path.Combine(env.WebRootPath, "images", fileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        productmod.Image.CopyTo(fileStream);
                    }

                    urlImage = (Utils.GetBaseUrl(HttpContext.Request) + "/images/" + fileName);
                    productmod.URLImage = urlImage;
                }
            }

            //Save in db
            Product product = new Product()
            {
                Sku = productmod.Sku,
                Name = productmod.Name,
                Description = productmod.Description,
                Quantity = productmod.Quantity,
                Price = productmod.Price,
                URLImage = urlImage
            };
            db.Products.Add(product);
            db.SaveChanges();

            //Return to value
            return new CreatedAtRouteResult("GetProduct", new { id = product.Sku }, product);

        }


        // Update products
        // PUT: api/products/{id}
        [HttpPut("{id}")]
        public IActionResult UpdateProduct(string id, [FromForm] ProductViewModel productmod)
        {
            // Verify model is not valid
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // Verify same id
            if (productmod.Sku != id)
            {
                ModelState.AddModelError("Update Error", "Product id does not match");
                return BadRequest(ModelState);
            }

            //Get the db model
            Product product = db.Products.FirstOrDefault(f => f.Sku.Equals(id));
            if (product == null)
            {
                return NotFound();
            }

            //Save in db
            product.Sku = productmod.Sku;
            product.Name = productmod.Name;
            product.Description = productmod.Description;
            product.Quantity = productmod.Quantity;
            product.Price = productmod.Price;

            //Verify file exists
            if (productmod.Image != null)
            {
                if (productmod.Image.Length > 0)
                {
                    //Save file
                    var fileName = Path.GetFileName(productmod.Image.FileName);
                    var filePath = Path.Combine(env.WebRootPath, "images", fileName);
                    using (var fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        productmod.Image.CopyTo(fileStream);
                    }

                    product.URLImage = (Utils.GetBaseUrl(HttpContext.Request) + "/images/" + fileName);
                    productmod.URLImage = product.URLImage;
                }
            }
            else
            {
                productmod.URLImage = product.URLImage;
            }

            //Update in db
            db.Entry(product).State = EntityState.Modified;
            db.SaveChanges();

            //Return 
            return Ok(productmod);
        }


        // Delete product
        // DELETE: /api/products/{id}
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(string id)
        {
            //Get product
            var product = db.Products.FirstOrDefault(f => f.Sku.Equals(id));

            //Product not found
            if (product == null)
            {
                return NotFound();
            }

            //Get Product model
            var productmodel = new ProductViewModel()
            {
                Sku = product.Sku,
                Name = product.Name,
                Description = product.Description,
                Quantity = product.Quantity,
                Price = product.Price,
                URLImage = product.URLImage
            };

            //Delete from DB
            db.Remove(product);
            db.SaveChanges();

            //Return
            return Ok(productmodel);
        }

    }
}

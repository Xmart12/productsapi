﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ProductsAPI.Helpers;
using ProductsAPI.Models;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ProductsAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/account")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)] 
    [ApiController]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IConfiguration _configuration;
        private readonly ApplicationDbContext db;


        //Constructor
        public AccountController(UserManager<ApplicationUser> userManager,
            RoleManager<ApplicationRole> roleManager,
            SignInManager<ApplicationUser> signInManager,
            IConfiguration configuration,
            ApplicationDbContext context)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _signInManager = signInManager;
            _configuration = configuration;
            db = context;
        }



        // Create User
        // POST /api/account/create
        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterUser([FromBody] RegisterModel model)
        {
            //Verify model is valid
            if (ModelState.IsValid)
            {
                //Check for roles
                await ChekRoles();

                //Verify Email
                if (db.UserInfos.Count(c => c.Email.Equals(model.Email)) > 0)
                {
                    //Failure creating user
                    ModelState.AddModelError("Account Error", "Email is already used");
                    return BadRequest(ModelState);
                }

                //Create User
                var user = new ApplicationUser { UserName = model.UserName, Email = model.Email };
                var result = await _userManager.CreateAsync(user, model.Password);

                //Verify User Creation succeed
                if (result.Succeeded)
                {
                    //Save UserInfo
                    UserInfo userInfo = new UserInfo()
                    {
                        Id = user.Id,
                        Email = user.Email,
                        UserName = user.UserName,
                        Name = model.Name,
                        Role = RoleType.User,
                        PhoneNumber = model.PhoneNumber,
                        BirthDate = model.BirthDate
                    };
                    await db.UserInfos.AddAsync(userInfo);
                    await db.SaveChangesAsync();

                    //Add Role to user
                    await _userManager.AddToRoleAsync(user, RoleType.User);

                    //Send Confirm email
                    await SendConfirmEmail(user);

                    //Return message OK
                    return Ok(new { message = "Account registed, please check your email to confirm your account" });
                }
                else
                {
                    //Failure creating user
                    var listError = result.Errors.Select(s => s.Description).ToList();
                    listError.Add("Username or password invalid");
                    return BadRequest(listError);
                }
            }
            else
            {
                //Return failure
                return BadRequest(ModelState);
            }
        }


        // Login
        // POST /api/account/login
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginModel login)
        {
            //Model is valid
            if (ModelState.IsValid)
            {
                //Login attemp
                var result = await _signInManager.PasswordSignInAsync(login.UserName, login.Password, isPersistent: false, lockoutOnFailure: false);

                //Verify succeed
                if (result.Succeeded)
                {
                    //Get user
                    ApplicationUser user = await _userManager.FindByNameAsync(login.UserName);

                    //Verify user email confirm
                    if (!user.EmailConfirmed)
                    {
                        ModelState.AddModelError("Account Error", "Account not confirmed, please, confirm your account");
                        return BadRequest(ModelState);
                    }

                    //Get user info
                    var userInfo = db.UserInfos.FirstOrDefault(f => f.UserName.Equals(login.UserName));

                    //Generate token and return
                    return BuildToken(userInfo);
                }
                else
                {
                    //Login Failure
                    ModelState.AddModelError("Account Error", "Invalid login attempt.");
                    return BadRequest(ModelState);
                }
            }
            else
            {
                //Return Failure
                return BadRequest(ModelState);
            }
        }


        // Confirm Account
        // GET /api/account/confirm?userid&code
        [HttpGet("confirm")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmUser(string userid, string code)
        {
            //Verify paramaters
            if (userid == null && code == null)
            {
                ModelState.AddModelError("Account Error", "Confirmation token invalid");
                return BadRequest(ModelState);
            }

            //Get and verify user exists
            ApplicationUser user = await _userManager.FindByIdAsync(userid);
            if (user == null)
            {
                ModelState.AddModelError("Account Error", "User not found");
                return BadRequest(ModelState);
            }

            //Confirm Email account
            var result = await _userManager.ConfirmEmailAsync(user, code);
            if (result.Succeeded)
            {
                //Return OK
                return Ok(new { message = "Account Confirmed successfully" });
            }
            else
            {
                //Return Failure
                var errorList = result.Errors.Select(s => s.Description).ToList();
                errorList.Add("Error confirming account");
                return BadRequest(errorList);
            }
        }


        // Send Confirm Email
        // POST /api/account/sendconfirm
        [HttpPost("sendconfirm")]
        [AllowAnonymous]
        public async Task<IActionResult> SendConfirmEmail([FromBody] ConfirmAccountModel confirm)
        {
            //Check use is valid
            if (ModelState.IsValid)
            {
                //Get and verify user exists
                ApplicationUser user = await _userManager.FindByEmailAsync(confirm.Email);
                if (user == null)
                {
                    ModelState.AddModelError("Account Error", "Account not found");
                    return BadRequest(ModelState);
                }

                //Send confirm mail
                await SendConfirmEmail(user);

                //Return OK
                return Ok(new { message = "Confirm email sended, please check your email to confirm your account" });
            }
            else
            {
                //Return Failure
                return BadRequest(ModelState);
            }
        }


        // Forgot Password 
        // POST /api/account/forgotpassword
        [HttpPost("forgotpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassowrd([FromBody] ConfirmAccountModel reset)
        {
            //Check user model is valid
            if (ModelState.IsValid)
            {
                //Get and verify user exist
                ApplicationUser user = await _userManager.FindByEmailAsync(reset.Email);
                if (user == null)
                {
                    ModelState.AddModelError("Account Error", "Account not found");
                    return BadRequest(ModelState);
                }

                //Send reset mail
                await SendResetPassword(user);

                //Return OK
                return Ok(new { message = "Reset password account email sended, please check your email to reset your password account" });
            }
            else
            {
                //Return Failure
                return BadRequest(ModelState);
            }
        }


        // Reset Password (Initial)
        // GET /api/account/resetpassword?userid&code
        [HttpGet("resetpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(string userid, string code)
        {

            //Verify parameters
            if (userid == null && code == null)
            {
                ModelState.AddModelError("Account Error", "Reset password token invalid");
                return BadRequest(ModelState);
            }

            //Get and verify user exist
            ApplicationUser user = await _userManager.FindByIdAsync(userid);
            if (user == null)
            {
                ModelState.AddModelError("Account Error", "User not found");
                return BadRequest(ModelState);
            }

            //Return OK
            return Ok(new { email = user.Email, code = code });
        }


        // Reset Password (Confirmation)
        // POST /api/account/resetpassword
        [HttpPost("resetpassword")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel resetPassword)
        {
            //Check model is valid
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //Get and verify user exists
            ApplicationUser user = await _userManager.FindByEmailAsync(resetPassword.Email);
            if (user == null)
            {
                ModelState.AddModelError("Account Error", "User not found");
                return BadRequest(ModelState);
            }

            //Reset Password
            var result = await _userManager.ResetPasswordAsync(user, resetPassword.Code, resetPassword.Password);
            if (result.Succeeded)
            {
                //Return OK
                return Ok(new { message = "Password reseted successfully." });
            }
            else
            {
                //Return Failure
                var errorList = result.Errors.Select(s => s.Description).ToList();
                errorList.Add("Reset password failed");
                return BadRequest(errorList);
            }
        }


        // Change role to users by Admin
        // POST /api/account/changerole
        [HttpPost("changerole")]
        [Authorize(Roles = RoleType.Admin)]
        public async Task<IActionResult> ChangeRoleUser([FromBody] ChangeRole changeRole)
        {
            //Verify model is valid
            if (ModelState.IsValid)
            {
                //Get and verify user exists
                ApplicationUser user = await _userManager.FindByEmailAsync(changeRole.Email);
                if (user == null)
                {
                    ModelState.AddModelError("Account Error", "User not Found");
                    return BadRequest(ModelState);
                }

                //Verify role exist
                ApplicationRole role = await _roleManager.FindByNameAsync(changeRole.Role);
                if (role == null)
                {
                    ModelState.AddModelError("Account Error", "Role not found");
                    return BadRequest(ModelState);
                }

                //Get user info
                UserInfo userInfo = db.UserInfos.FirstOrDefault(f => f.Id.Equals(user.Id));

                //Remove last role
                var resRemove = await _userManager.RemoveFromRoleAsync(user, userInfo.Role);
                if (!resRemove.Succeeded)
                {
                    //Return failure
                    var errorlist = resRemove.Errors.Select(s => s.Description).ToList();
                    errorlist.Add("Change role failure");
                    return BadRequest(errorlist);
                }

                //Add new role
                var result = await _userManager.AddToRoleAsync(user, changeRole.Role);
                if (result.Succeeded)
                {
                    //Update user info
                    userInfo.Role = changeRole.Role;
                    db.Entry(userInfo).State = EntityState.Modified;
                    db.SaveChanges();

                    //Return OK
                    return Ok(new { message = "New role added to user successfully" });
                }
                else
                {
                    //Return failure
                    var errorlist = result.Errors.Select(s => s.Description).ToList();
                    errorlist.Add("Change role failure");
                    return BadRequest(errorlist);
                }
            }
            else
            {
                //Return Failure
                return BadRequest(ModelState);
            }
        }



        #region Functions



        //Token Creation
        private IActionResult BuildToken(UserInfo userInfo)
        {
            //Set claims
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.UniqueName, userInfo.Email),
                new Claim(JwtRegisteredClaimNames.Sid, userInfo.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.Role, userInfo.Role),
            };

            //Secret password
            var secretkey = _configuration["generalSettings:Security:JWTKey"];

            //Set credential keys
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretkey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //Set token expiration time
            var expiration = DateTime.UtcNow.AddHours(1);

            //Create token
            JwtSecurityToken token = new JwtSecurityToken(
               issuer: _configuration["generalSettings:Security:ValidIssuer"],
               audience: _configuration["generalSettings:Security:ValidAudience"],
               claims: claims,
               expires: expiration,
               signingCredentials: creds);

            // Return success with token
            return Ok(new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = expiration
            });

        }


        // Check for roles 
        private async Task ChekRoles()
        {
            //Create Role Admin if it does not exist
            var adminRole = await _roleManager.FindByNameAsync(RoleType.Admin);
            if (adminRole == null)
            {
                await _roleManager.CreateAsync(new ApplicationRole() { Name = RoleType.Admin });
            }

            //Create Role User if it does not exist
            var userRole = await _roleManager.FindByNameAsync(RoleType.User);
            if (userRole == null)
            {
                await _roleManager.CreateAsync(new ApplicationRole() { Name = RoleType.User });
            }

            //Create Admin role
            await CreateAdminRole();
        }


        //Create admin user
        private async Task CreateAdminRole()
        {
            //Chek for admin exists
            string userName = "admin@productsapi.com";
            var admin = db.UserInfos.FirstOrDefault(f => f.Email.Equals(userName));

            if (admin == null)
            {
                //Create user
                var adminUser = new ApplicationUser { UserName = userName, Email = userName };
                await _userManager.CreateAsync(adminUser, "Admin@123");

                //Add Role to admin user
                await _userManager.AddToRoleAsync(adminUser, RoleType.Admin);
                await _userManager.SetLockoutEnabledAsync(adminUser, false);

                //Confirm email
                var token = await _userManager.GenerateEmailConfirmationTokenAsync(adminUser);
                await _userManager.ConfirmEmailAsync(adminUser, token);

                //Save user info
                UserInfo user = new UserInfo()
                {
                    Id = adminUser.Id,
                    Email = adminUser.Email,
                    Name = "Admin User",
                    Role = RoleType.Admin
                };

                //Save to db
                await db.UserInfos.AddAsync(user);
                await db.SaveChangesAsync();
            }
        }


        //Send Confirm Account Function
        private async Task<bool> SendConfirmEmail(ApplicationUser user)
        {
            //Generate token
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

            //Encode parameters
            var htmlcode = HttpUtility.UrlEncode(code);
            var userid = HttpUtility.UrlEncode(user.Id);

            //Get url for confirm 
            var urlConfirm = Utils.GetBaseUrl(HttpContext.Request) + $"/api/account/confirm?userid={userid}&code={htmlcode}";

            //Build message
            string message = "<h1>Confirm your account</h1><br/>" +
                "<p>You must follow this link to activate your account:</p>" +
                $"<p><a href=\"{urlConfirm}\">Confirm Link</a></p>" +
                "<br/><font color='Red'>ProductAPI App</font>";

            //Send email
            return MailService.SendMail(user.Email, "Confirm your ProductAPI account", message);
        }


        //Send Reset Password Token Function
        private async Task<bool> SendResetPassword(ApplicationUser user)
        {
            //Generate token
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);

            //Encode parameters
            var htmlcode = HttpUtility.UrlEncode(code);
            var userid = HttpUtility.UrlEncode(user.Id);

            //Get url for reset
            var urlReset = Utils.GetBaseUrl(HttpContext.Request) + $"/api/account/resetpassword?userid={userid}&code={htmlcode}";

            //Build Message
            string message = "<h1>Reset your password</h1><br/>" +
                "<p>You must follow this link to reset your password account:</p>" +
                $"<p><a href=\"{urlReset}\">Confirm Link</a></p>" +
                "<br/><font color='Red'>ProductAPI App</font>";

            //Send Email
            return MailService.SendMail(user.Email, "Reset your password account", message);
        }



        #endregion



    }
}

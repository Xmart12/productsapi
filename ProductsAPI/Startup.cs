﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ProductsAPI.Helpers;
using ProductsAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductsAPI
{
    public class Startup
    {
        //Constructor
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        //Configurations
        public IConfiguration Configuration { get; }



        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1).AddJsonOptions(ConfigureJson);

            //Enable CORS
            services.AddCors(options => options.AddDefaultPolicy(builder => { builder.AllowAnyOrigin()
                .AllowAnyMethod().AllowAnyHeader(); }));

            //Add DB Context to services 
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(Configuration
            .GetConnectionString("defaultConnection")), ServiceLifetime.Transient);
            //services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("productsDB"));

            //Add Identity to services
            services.AddIdentity<ApplicationUser, ApplicationRole>().AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            //SecretJKey
            var secretkey = Configuration["generalSettings:Security:JWTKey"];

            //Set Mail Variables
            MailService.UserMail = Configuration["generalSettings:Mail:userMail"];
            MailService.UserPassword = Configuration["generalSettings:Mail:paswword"];
            MailService.FromMail = Configuration["generalSettings:Mail:FromMail"];
            MailService.DisplayName = Configuration["generalSettings:Mail:DisplayName"];
            MailService.MailServer = Configuration["generalSettings:Mail:MailServer"];
            MailService.MailPort = Convert.ToInt32(Configuration["generalSettings:Mail:MailPort"]);
            MailService.EnableSSL = Convert.ToBoolean(Configuration["generalSettings:Mail:EnableSSL"]);


            //Add Aunthetication to services
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options => options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["generalSettings:Security:ValidIssuer"],
                    ValidAudience = Configuration["generalSettings:Security:ValidAudience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretkey)),
                    ClockSkew = TimeSpan.Zero
                });
        }


        //Prevent loop reference
        private void ConfigureJson(MvcJsonOptions obj)
        {
            obj.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, ApplicationDbContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseStaticFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images")),
                RequestPath = "/images"
            });


            app.UseHttpsRedirection();
            app.UseMvc();
            app.UseAuthentication();
            app.UseCors();
        }


    }
}

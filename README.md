# ProductsAPI

A basic API for manage products - Elaniin Test

## Getting Started

This project contains a solution for the challenge test in Elaniin. It consists in a RESTFul Web API for products. These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Technologies used

* Web API: ASP.NET Core v2.1
* Database: SQL Server
* Packages: EntityFrameworkCore, EntityFrameworkCore.SqlServer (for data modeling)

### Database restore

* In Resources folder is included a Database BK and Script for restore
* Also is posible restore database with Code First with the command in Package Manager ```update-database```

### Application Settings

* Set connectionString in ```appsettings.json``` for example:

``` 
"ConnectionStrings": { "defaultConnection": 
	"Data Source=SERVER;User ID=USER;Password=PASSWORD;Connect Timeout=60;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False"
},  
```

* Set Secure JWT Key and mail services

```
"generalSettings": {
    "Security": {
      "JWTKey": "secretkey",
      "ValidIssuer": "elaniin",
      "ValidAudience":  "elaniin"
    },
    "Mail": {
      "userMail": "usermail@mail.com",
      "paswword": "",
      "FromMail": "noreply@productsapi.com",
      "DisplayName": "",
      "MailServer": "",
      "MailPort": "0",
      "EnableSSL": "False"
    }
  },
```

### Deployment on Heroku with Docker

Configure Dockerfile with this

```

FROM mcr.microsoft.com/dotnet/core/aspnet:2.1-stretch-slim AS base
WORKDIR /app
COPY . .

CMD ASPNETCORE_URLS=http://*:$PORT dotnet ProductsAPI.dll

```

Build project for publishing 

```

dotnet publish -c Release

```

Types this commands to execute the app on Heroku

```
heroku login
docker build -t heroku-name-app "PUBLISH_PROJECT_PATH"
docker tag heroku-name-app registry.heroku.com/heroku-name-app/web
heroku container:login
docker push registry.heroku.com/heroku-name-app/web
heroku container:release web --app heroku-name-app

```

### Heroku Publish

For API

```
https://productsapitest.herokuapp.com/api/products
```


### Postman Collection

In the resource folder is a postman collection for testing, use the base URL

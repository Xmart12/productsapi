-- --------------------------------------------------------
-- Host:                         den1.mssql7.gear.host
-- Versión del servidor:         Microsoft SQL Server 2017 (RTM-CU16) (KB4508218) - 14.0.3223.3
-- SO del servidor:              Windows Server 2016 Standard 10.0 <X64> (Build 14393: ) (Hypervisor)
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para productselaniin
CREATE DATABASE IF NOT EXISTS "productselaniin";
USE "productselaniin";

-- Volcando estructura para tabla productselaniin.AspNetRoleClaims
CREATE TABLE IF NOT EXISTS "AspNetRoleClaims" (
	"Id" INT(10,0) NOT NULL,
	"RoleId" NVARCHAR(450) NOT NULL,
	"ClaimType" NVARCHAR(max) NULL DEFAULT NULL,
	"ClaimValue" NVARCHAR(max) NULL DEFAULT NULL,
	PRIMARY KEY ("Id")
);

-- Volcando datos para la tabla productselaniin.AspNetRoleClaims: -1 rows
/*!40000 ALTER TABLE "AspNetRoleClaims" DISABLE KEYS */;
/*!40000 ALTER TABLE "AspNetRoleClaims" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.AspNetRoles
CREATE TABLE IF NOT EXISTS "AspNetRoles" (
	"Id" NVARCHAR(450) NOT NULL,
	"Name" NVARCHAR(256) NULL DEFAULT NULL,
	"NormalizedName" NVARCHAR(256) NULL DEFAULT NULL,
	"ConcurrencyStamp" NVARCHAR(max) NULL DEFAULT NULL,
	PRIMARY KEY ("Id")
);

-- Volcando datos para la tabla productselaniin.AspNetRoles: -1 rows
/*!40000 ALTER TABLE "AspNetRoles" DISABLE KEYS */;
INSERT INTO "AspNetRoles" ("Id", "Name", "NormalizedName", "ConcurrencyStamp") VALUES
	('14ad638b-13fe-4e11-96e7-70df5dba2ecb', 'User', 'USER', '0935334f-b7fb-41d0-93ad-6f2dbdb7dbdf'),
	('80c4f606-0207-486e-bd7d-c43382aad7fe', 'Admin', 'ADMIN', '57d28ed8-8a38-4b1b-993b-3dab3c930664');
/*!40000 ALTER TABLE "AspNetRoles" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.AspNetUserClaims
CREATE TABLE IF NOT EXISTS "AspNetUserClaims" (
	"Id" INT(10,0) NOT NULL,
	"UserId" NVARCHAR(450) NOT NULL,
	"ClaimType" NVARCHAR(max) NULL DEFAULT NULL,
	"ClaimValue" NVARCHAR(max) NULL DEFAULT NULL,
	PRIMARY KEY ("Id")
);

-- Volcando datos para la tabla productselaniin.AspNetUserClaims: -1 rows
/*!40000 ALTER TABLE "AspNetUserClaims" DISABLE KEYS */;
/*!40000 ALTER TABLE "AspNetUserClaims" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.AspNetUserLogins
CREATE TABLE IF NOT EXISTS "AspNetUserLogins" (
	"LoginProvider" NVARCHAR(450) NOT NULL,
	"ProviderKey" NVARCHAR(450) NOT NULL,
	"ProviderDisplayName" NVARCHAR(max) NULL DEFAULT NULL,
	"UserId" NVARCHAR(450) NOT NULL,
	PRIMARY KEY ("LoginProvider","ProviderKey")
);

-- Volcando datos para la tabla productselaniin.AspNetUserLogins: -1 rows
/*!40000 ALTER TABLE "AspNetUserLogins" DISABLE KEYS */;
/*!40000 ALTER TABLE "AspNetUserLogins" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.AspNetUserRoles
CREATE TABLE IF NOT EXISTS "AspNetUserRoles" (
	"UserId" NVARCHAR(450) NOT NULL,
	"RoleId" NVARCHAR(450) NOT NULL,
	PRIMARY KEY ("UserId","RoleId")
);

-- Volcando datos para la tabla productselaniin.AspNetUserRoles: -1 rows
/*!40000 ALTER TABLE "AspNetUserRoles" DISABLE KEYS */;
INSERT INTO "AspNetUserRoles" ("UserId", "RoleId") VALUES
	('aa51e428-2170-45b3-9140-44e8c6bc30d0', '14ad638b-13fe-4e11-96e7-70df5dba2ecb'),
	('3f625033-eaf1-40a1-b04b-6365edaa350a', '80c4f606-0207-486e-bd7d-c43382aad7fe');
/*!40000 ALTER TABLE "AspNetUserRoles" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.AspNetUsers
CREATE TABLE IF NOT EXISTS "AspNetUsers" (
	"Id" NVARCHAR(450) NOT NULL,
	"UserName" NVARCHAR(256) NULL DEFAULT NULL,
	"NormalizedUserName" NVARCHAR(256) NULL DEFAULT NULL,
	"Email" NVARCHAR(256) NULL DEFAULT NULL,
	"NormalizedEmail" NVARCHAR(256) NULL DEFAULT NULL,
	"EmailConfirmed" BIT NOT NULL,
	"PasswordHash" NVARCHAR(max) NULL DEFAULT NULL,
	"SecurityStamp" NVARCHAR(max) NULL DEFAULT NULL,
	"ConcurrencyStamp" NVARCHAR(max) NULL DEFAULT NULL,
	"PhoneNumber" NVARCHAR(max) NULL DEFAULT NULL,
	"PhoneNumberConfirmed" BIT NOT NULL,
	"TwoFactorEnabled" BIT NOT NULL,
	"LockoutEnd" DATETIMEOFFSET(7) NULL DEFAULT NULL,
	"LockoutEnabled" BIT NOT NULL,
	"AccessFailedCount" INT(10,0) NOT NULL,
	PRIMARY KEY ("Id")
);

-- Volcando datos para la tabla productselaniin.AspNetUsers: -1 rows
/*!40000 ALTER TABLE "AspNetUsers" DISABLE KEYS */;
INSERT INTO "AspNetUsers" ("Id", "UserName", "NormalizedUserName", "Email", "NormalizedEmail", "EmailConfirmed", "PasswordHash", "SecurityStamp", "ConcurrencyStamp", "PhoneNumber", "PhoneNumberConfirmed", "TwoFactorEnabled", "LockoutEnd", "LockoutEnabled", "AccessFailedCount") VALUES
	('3f625033-eaf1-40a1-b04b-6365edaa350a', 'admin@productsapi.com', 'ADMIN@PRODUCTSAPI.COM', 'admin@productsapi.com', 'ADMIN@PRODUCTSAPI.COM', b'1', 'AQAAAAEAACcQAAAAEGSN1jVCpLYi75In3UBLRP0Khe+RTCd+T7Tl6fwunQAqhlVEj8wbBLqgU8TQaefpWQ==', 'HM4T5HYCBSY2NBKDAT22FWOL4OWG5QXL', '00a324e1-bbb8-4c3a-b6df-dec5913544f5', NULL, b'0', b'0', NULL, b'0', 0),
	('aa51e428-2170-45b3-9140-44e8c6bc30d0', 'xmart', 'XMART', 'stanleysmart12@gmail.com', 'STANLEYSMART12@GMAIL.COM', b'1', 'AQAAAAEAACcQAAAAEGext1JlSyZBXfybW4wX/2lDC9MYlSMUTLfSuxd0hwR+qjtCCQaCqceSAy29g1QfBg==', 'C5NKHGOD7JXRGJIDXOWBEL5MDKGGXLZE', '40d105aa-c384-4e38-8407-44c8f044f716', NULL, b'0', b'0', NULL, b'1', 0);
/*!40000 ALTER TABLE "AspNetUsers" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.AspNetUserTokens
CREATE TABLE IF NOT EXISTS "AspNetUserTokens" (
	"UserId" NVARCHAR(450) NOT NULL,
	"LoginProvider" NVARCHAR(450) NOT NULL,
	"Name" NVARCHAR(450) NOT NULL,
	"Value" NVARCHAR(max) NULL DEFAULT NULL,
	PRIMARY KEY ("UserId","LoginProvider","Name")
);

-- Volcando datos para la tabla productselaniin.AspNetUserTokens: -1 rows
/*!40000 ALTER TABLE "AspNetUserTokens" DISABLE KEYS */;
/*!40000 ALTER TABLE "AspNetUserTokens" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.Products
CREATE TABLE IF NOT EXISTS "Products" (
	"Sku" NVARCHAR(450) NOT NULL,
	"Name" NVARCHAR(max) NOT NULL,
	"Quantity" FLOAT(53) NOT NULL,
	"Price" FLOAT(53) NOT NULL,
	"Description" NVARCHAR(max) NULL DEFAULT NULL,
	"URLImage" NVARCHAR(max) NULL DEFAULT NULL,
	PRIMARY KEY ("Sku")
);

-- Volcando datos para la tabla productselaniin.Products: -1 rows
/*!40000 ALTER TABLE "Products" DISABLE KEYS */;
INSERT INTO "Products" ("Sku", "Name", "Quantity", "Price", "Description", "URLImage") VALUES
	('SN001', 'Sandwich Chicken', 5, 2.5, 'Subway Sandwich Chicken', 'https://localhost:5001/images/png-transparent-submarine-sandwich-panini-delicatessen-italian-sandwich-bread-furniture-food-rectangle.png'),
	('SN002', 'Sandwich Turkey', 2, 2, 'Subway Sandwich Turkey', 'https://localhost:5001/images/png-transparent-submarine-sandwich-panini-delicatessen-italian-sandwich-bread-furniture-food-rectangle.png'),
	('SN003', 'Sandwich Veggetarian', 7, 1.75, 'Subway Sandwich Veggetarian', 'https://localhost:5001/images/png-transparent-submarine-sandwich-panini-delicatessen-italian-sandwich-bread-furniture-food-rectangle.png'),
	('SN004', 'Sandwich Teriyaki', 3, 3.25, 'Subway Sandwich Teriyaki', 'https://localhost:5001/images/png-transparent-submarine-sandwich-panini-delicatessen-italian-sandwich-bread-furniture-food-rectangle.png'),
	('SN005', 'Sandwich Salami', 9, 2.75, 'Subway Sandwich Salami', 'https://localhost:5001/images/png-transparent-submarine-sandwich-panini-delicatessen-italian-sandwich-bread-furniture-food-rectangle.png'),
	('SN006', 'Sandwich Bacon', 1, 2.5, 'Subway Sandwich Bacon', 'https://localhost:5001/images/png-transparent-submarine-sandwich-panini-delicatessen-italian-sandwich-bread-furniture-food-rectangle.png');
/*!40000 ALTER TABLE "Products" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.UserInfos
CREATE TABLE IF NOT EXISTS "UserInfos" (
	"Id" NVARCHAR(450) NOT NULL,
	"UserName" NVARCHAR(max) NULL DEFAULT NULL,
	"Email" NVARCHAR(max) NULL DEFAULT NULL,
	"Name" NVARCHAR(max) NULL DEFAULT NULL,
	"PhoneNumber" NVARCHAR(max) NULL DEFAULT NULL,
	"BirthDate" DATETIME2(7) NOT NULL,
	"Role" NVARCHAR(max) NULL DEFAULT NULL,
	PRIMARY KEY ("Id")
);

-- Volcando datos para la tabla productselaniin.UserInfos: -1 rows
/*!40000 ALTER TABLE "UserInfos" DISABLE KEYS */;
INSERT INTO "UserInfos" ("Id", "UserName", "Email", "Name", "PhoneNumber", "BirthDate", "Role") VALUES
	('3f625033-eaf1-40a1-b04b-6365edaa350a', NULL, 'admin@productsapi.com', 'Admin User', NULL, '0001-01-01 00:00:00.0000000', 'Admin'),
	('aa51e428-2170-45b3-9140-44e8c6bc30d0', 'xmart', 'stanleysmart12@gmail.com', 'Name', '123', '1993-04-16 00:00:00.0000000', 'User');
/*!40000 ALTER TABLE "UserInfos" ENABLE KEYS */;

-- Volcando estructura para tabla productselaniin.__EFMigrationsHistory
CREATE TABLE IF NOT EXISTS "__EFMigrationsHistory" (
	"MigrationId" NVARCHAR(150) NOT NULL,
	"ProductVersion" NVARCHAR(32) NOT NULL,
	PRIMARY KEY ("MigrationId")
);

-- Volcando datos para la tabla productselaniin.__EFMigrationsHistory: -1 rows
/*!40000 ALTER TABLE "__EFMigrationsHistory" DISABLE KEYS */;
INSERT INTO "__EFMigrationsHistory" ("MigrationId", "ProductVersion") VALUES
	('20201129015811_Initial_Migration', '2.1.14-servicing-32113');
/*!40000 ALTER TABLE "__EFMigrationsHistory" ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
